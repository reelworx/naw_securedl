<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(function($extKey) {
	$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['tx_nawsecuredl'] = 'EXT:naw_securedl/Resources/Private/Scripts/FileDeliveryEidDispatcher.php';

	\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\SignalSlot\\Dispatcher')->connect(
		'TYPO3\\CMS\\Core\\Resource\\ResourceStorage',
		\TYPO3\CMS\Core\Resource\ResourceStorage::SIGNAL_PreGeneratePublicUrl,
		'Bitmotion\\NawSecuredl\\Resource\\UrlGenerationInterceptor',
		'getPublicUrl'
	);

	$configurationManager = new \Bitmotion\NawSecuredl\Configuration\ConfigurationManager();

	if ($configurationManager->getValue('apacheDelivery')) {
		if (TYPO3_MODE === 'FE') {
			$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['checkDataSubmission']['naw_securedl_set_access_token_cookie'] = 'Bitmotion\\NawSecuredl\\Security\\Authorization\\Resource\\AccessTokenCookiePublisher';
		}
	} else {
		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['contentPostProc-output'][] = 'Bitmotion\\NawSecuredl\\Service\\SecureDownloadService->parseFE';
	}

	$objectContainer = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class);
	$objectContainer->registerImplementation(
		'Bitmotion\\NawSecuredl\\Resource\\Publishing\\ResourcePublishingTargetInterface',
		'Bitmotion\\NawSecuredl\\Resource\\Publishing\\PhpDeliveryProtectedResourcePublishingTarget'
	);

}, $_EXTKEY);
