<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(function($extKey) {

	$_EXTCONF = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$extKey]);

	if (TYPO3_MODE === 'BE' && !empty($_EXTCONF['log'])) {
		\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addModule(
			'tools',
			'txnawsecuredlM1',
			'',
			\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($extKey) . 'modLog/'
		);
	}

}, $_EXTKEY);
