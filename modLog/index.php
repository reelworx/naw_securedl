<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2009 Helmut Hummel <typo3-ext(at)bitmotion.de>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

$LANG->includeLLFile('EXT:naw_securedl/modLog/locallang.xml');
$BE_USER->modAccess($MCONF,1);    // This checks permissions and exits if the users has no permission for entry.

/**
 * Module 'Download Log' for the 'naw_securedl' extension.
 *
 * @author    Helmut Hummel <typo3-ext(at)bitmotion.de>
 * @package    TYPO3
 * @subpackage    tx_nawsecuredl
 */
class tx_nawsecuredl_module1 extends \TYPO3\CMS\Backend\Module\BaseScriptClass {

	var $pageinfo;

	/**
	 * Adds items to the ->MOD_MENU array. Used for the function menu selector.
	 *
	 * @return    void
	 */
	function menuConfig()    {
		global $LANG;

		$this->MOD_MENU = array (
			'users' => array (
				'-1' => $LANG->getLL('loggedInUsers'),
				'0' => $LANG->getLL('notLoggedIn'),
				'' => '------------------------------',
			),
			'mode' => array (
				'-1' => $LANG->getLL('allTime'),
				'1' => $LANG->getLL('byTime'),
			),
		);

		foreach (\TYPO3\CMS\Backend\Utility\BackendUtility::getRecordsByField('fe_users', 1, 1) as $user) {
			$this->MOD_MENU['users'][$user['uid']] = $user['username'];
		}

		parent::menuConfig();

		$set = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('SET');

		if ($set['time']) {
			$dateFrom = strtotime($set['time']['from']);
			$dateTo = strtotime($set['time']['to']);

			$set['time']['from'] = ($dateFrom > 0) ? date('d.m.Y', $dateFrom) : '';
			$set['time']['to'] = ($dateTo > 0) ? date('d.m.Y', $dateTo) : '';

			$mergedSettings = \TYPO3\CMS\Core\Utility\GeneralUtility::array_merge($this->MOD_SETTINGS, $set);

			$GLOBALS['BE_USER']->pushModuleData($this->MCONF['name'], $mergedSettings);
			$this->MOD_SETTINGS = $mergedSettings;
		}
	}

	/**
	 * Main function of the module. Write the content to $this->content
	 * If you chose "web" as main module, you will need to consider the $this->id parameter which will contain the uid-number of the page clicked in the page tree
	 */
	function main()
	{
		global $BE_USER,$LANG,$BACK_PATH,$TCA_DESCR,$TCA,$CLIENT,$TYPO3_CONF_VARS;

		// Access check!
		// The page will show only if there is a valid page and if this page may be viewed by the user
		$this->pageinfo = \TYPO3\CMS\Backend\Utility\BackendUtility::readPageAccess($this->id,$this->perms_clause);
		$access = is_array($this->pageinfo) ? 1 : 0;

		if (($this->id && $access) || ($BE_USER->user['admin'] && !$this->id))    {

				// Draw the header.
			$this->doc = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Backend\\Template\\DocumentTemplate');
			$this->doc->backPath = $BACK_PATH;
			$this->doc->form='<form action="" method="POST">';

				// JavaScript
			$this->doc->JScode = '
				<script language="javascript" type="text/javascript">
					script_ended = 0;
					function jumpToUrl(URL)    {
						document.location = URL;
					}
				</script>
			';
			$this->doc->postCode='
				<script language="javascript" type="text/javascript">
					script_ended = 1;
					if (top.fsMod) top.fsMod.recentIds["web"] = 0;
				</script>
			';

			$headerSection = $this->doc->getHeader('pages',$this->pageinfo,$this->pageinfo['_thePath']).'<br />'
							 .$LANG->sL('LLL:EXT:lang/locallang_core.xml:labels.path').': '
							 . \TYPO3\CMS\Core\Utility\GeneralUtility::fixed_lgd_cs($this->pageinfo['_thePath'],-50);

			$this->content.=$this->doc->startPage($LANG->getLL('title'));
			$this->content.=$this->doc->header($LANG->getLL('title'));
			$this->content.=$this->doc->spacer(5);
			$this->content.=$this->doc->section('',$this->doc->funcMenu($headerSection,\TYPO3\CMS\Backend\Utility\BackendUtility::getFuncMenu($this->id,'SET[mode]',$this->MOD_SETTINGS['mode'],$this->MOD_MENU['mode'],'index.php')));
			$this->content.=$this->doc->section('',$this->doc->funcMenu('',\TYPO3\CMS\Backend\Utility\BackendUtility::getFuncMenu($this->id,'SET[users]',$this->MOD_SETTINGS['users'],$this->MOD_MENU['users'],'index.php')));


			if (1 == (int)$this->MOD_SETTINGS['mode']) {
				$this->content.= '<br />
				<table cellspacing="0" cellpadding="0" border="0" width="100%" id="typo3-funcmenu">
				<tbody><tr>
					<td nowrap="nowrap" valign="top"/>
					<td nowrap="nowrap" align="right" valign="top">
					'.$LANG->getLL('from').':&nbsp;
					<input name="SET[time][from]" value="'.htmlspecialchars($this->MOD_SETTINGS['time']['from']).'" />
					</td>
				</tr>
				</tbody></table>
				';
				$this->content.= '<br />
				<table cellspacing="0" cellpadding="0" border="0" width="100%" id="typo3-funcmenu">
				<tbody><tr>
					<td nowrap="nowrap" valign="top"/>
					<td nowrap="nowrap" align="right" valign="top">
					'.$LANG->getLL('to').':&nbsp;
					<input name="SET[time][to]" value="'.htmlspecialchars($this->MOD_SETTINGS['time']['to']).'" />
					</td>
				</tr>
				</tbody></table>
				';
				$this->content.= '<br />
				<table cellspacing="0" cellpadding="0" border="0" width="100%" id="typo3-funcmenu">
				<tbody><tr>
					<td nowrap="nowrap" valign="top"/>
					<td nowrap="nowrap" align="right" valign="top">
					<input type="submit" value="'.$LANG->getLL('submit').'" />
					</td>
				</tr>
				</tbody></table>
				';
			}

			$this->content.=$this->doc->divider(5);


			// Render content:
			$this->moduleContent();


			// ShortCut
			if ($BE_USER->mayMakeShortcut())    {
				$this->content.=$this->doc->spacer(20).$this->doc->section('',$this->doc->makeShortcutIcon('id',implode(',',array_keys($this->MOD_MENU)),$this->MCONF['name']));
			}

			$this->content.=$this->doc->spacer(10);
		} else {
				// If no access or if ID == zero

			$this->doc = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('mediumDoc');
			$this->doc->backPath = $BACK_PATH;

			$this->content.=$this->doc->startPage($LANG->getLL('title'));
			$this->content.=$this->doc->header($LANG->getLL('title'));
			$this->content.=$this->doc->spacer(5);
			$this->content.=$this->doc->spacer(10);
		}
	}

	/**
	 * Prints out the module HTML
	 *
	 * @return    void
	 */
	function printContent()
	{
		$this->content.=$this->doc->endPage();
		echo $this->content;
	}

	/**
	 * Generates the module content
	 *
	 * @return    void
	 */
	function moduleContent()
	{
		if (-1 == $this->MOD_SETTINGS['mode']) {
			$time = null;
		} else {
			$time = $this->MOD_SETTINGS['time'];
		}
		$content = $this->getDownloadTrafficTable((int)$this->MOD_SETTINGS['users'], $time);
		$this->content.=$this->doc->section($GLOBALS['LANG']->getLL('trafficUsed').':',$content,0,1);
	}



	protected function getDownloadTrafficTable($userId, $time)
	{
		global $LANG;

		$dateFrom = strtotime($time['from']);
		$dateTo = strtotime($time['to']);


		$arrFromTables[] = 'tx_nawsecuredl_counter';
		$arrSelectFields[] = 'GROUP_CONCAT(DISTINCT tx_nawsecuredl_counter.file_name) AS files';
		$arrSelectFields[] = 'sum(tx_nawsecuredl_counter.file_size) AS traffic';
		$arrSelectFields[] = 'FROM_UNIXTIME(tx_nawsecuredl_counter.tstamp,\'%d.%m.%Y\') AS date';
		$arrGroupBy = array();
		$arrOrderBy = array();

		$arrAndConditions = array();

		if ($userId > 0 OR $userId == -1) {
			$arrOrderBy[] = 'username';
			$arrSelectFields[] = 'fe_users.username AS username';
			$arrAndConditions[] = 'fe_users.uid = tx_nawsecuredl_counter.user_id';
			$arrFromTables[] = 'fe_users';
			$arrGroupBy[] = 'username';
		}
		$arrOrderBy[] = 'tx_nawsecuredl_counter.tstamp';

		if ($userId != -1) {
			$arrAndConditions[] = 'tx_nawsecuredl_counter.user_id='.(int)$userId;
		}

		$arrGroupBy[] = 'date';

		if ($dateFrom > 0) {
			$arrAndConditions[] = 'tx_nawsecuredl_counter.tstamp >= '.$dateFrom;
		}
		if ($dateTo > 0) {
			$arrAndConditions[] = 'tx_nawsecuredl_counter.tstamp < '.($dateTo + 86400);
		}

		$rows = self::getRecords(implode(',', $arrFromTables), implode(',',$arrSelectFields), implode(' AND ', $arrAndConditions), implode(',', $arrGroupBy), implode(',', $arrOrderBy));

		if ($rows) {

			$table = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Bitmotion\\NawSecuredl\\Table');
			$table->setHeader(array($LANG->getLL('user'),$LANG->getLL('files'),$LANG->getLL('date'),$LANG->getLL('traffic')));

			$sum = 0;
			foreach ($rows as $row) {
				$table->addRow(array($this->HtmlEscape($row['username']), $this->FormatFiles($row['files']), $this->HtmlEscape($row['date']), $this->FormatTraffic($row['traffic'])), false);
				$sum += $row['traffic'];
			}
			$table->addRow(array('','','<strong>'.$LANG->getLL('trafficsum').':</strong>','<strong>'.$this->FormatTraffic($sum).'</strong>'), false);

		} else {
			return $LANG->getLL('noTrafficUsed');
		}
		return $table->render();
	}


	protected function FormatFiles($files)
	{
		$arrFiles = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',', $files);
		foreach ($arrFiles as &$file) {
			$file = $this->HtmlEscape(basename($file));
		}

		return implode('<br />', $arrFiles);
	}

	protected function FormatTraffic($value)
	{
		return $this->HtmlEscape(sprintf('%01.2F',(double)$value / (1024*1024*1024)));

	}

	private function HtmlEscape($string)
	{
		return htmlspecialchars($string, ENT_QUOTES, $GLOBALS['LANG']->charSet);
	}

	/**
	 * Returns records from table, $theTable, where a field ($theField) equals the value, $theValue
	 * The records are returned in an array
	 * If no records were selected, the function returns nothing
	 * Usage: 8
	 *
	 * @param	string	$theTable	Table name present in $TCA
	 * @param	string	$fields	Field to select on
	 * @param	string	$whereClause	Optional additional WHERE clauses put in the end of the query. DO NOT PUT IN GROUP BY, ORDER BY or LIMIT!
	 * @param	string	$groupBy	Optional GROUP BY field(s), if none, supply blank string.
	 * @param	string	$orderBy	Optional ORDER BY field(s), if none, supply blank string.
	 * @param	string	$limit	Optional LIMIT value ([begin,]max), if none, supply blank string.
	 * @param	boolean	$useDeleteClause	Use the deleteClause to check if a record is deleted (default true)
	 * @return	mixed		Multidimensional array with selected records (if any is selected)
	 */
	protected static function getRecords($theTable, $fields, $whereClause = '', $groupBy = '', $orderBy = '', $limit = '', $useDeleteClause = true)
	{
		#$GLOBALS['TYPO3_DB']->store_lastBuiltQuery = true;
		$res = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
					$fields ? $fields : '*',
					$theTable,
					($useDeleteClause ? \TYPO3\CMS\Backend\Utility\BackendUtility::deleteClause($theTable).' ' : '').
						\TYPO3\CMS\Backend\Utility\BackendUtility::versioningPlaceholderClause($theTable).' '.
						$whereClause,	// whereClauseMightContainGroupOrderBy
					$groupBy,
					$orderBy,
					$limit
				);
		$rows = array();
		while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
			$rows[] = $row;
		}
		#debug($GLOBALS['TYPO3_DB']->debug_lastBuiltQuery,"Debug of \$GLOBALS['TYPO3_DB']->debug_lastBuiltQuery"); 	//FIXME debug of $GLOBALS['TYPO3_DB']->debug_lastBuiltQuery
		$GLOBALS['TYPO3_DB']->sql_free_result($res);

		return $rows;
	}

}


// Make instance:
$SOBE = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('tx_nawsecuredl_module1');
$SOBE->init();

$SOBE->main();
$SOBE->printContent();
