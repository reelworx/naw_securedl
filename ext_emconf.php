<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "naw_securedl".
 *
 * Auto generated 05-03-2015 16:23
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Secure Downloads',
	'description' => '"Secure Download": Apply TYPO3 access rights to ALL file assets (PDFs, TGZs or JPGs etc. - configurable) - protect them from direct access.',
	'category' => 'fe',
	'version' => '2.0.0',
	'state' => 'stable',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearcacheonload' => 0,
	'author' => 'Dietrich Heise, Helmut Hummel, Markus Klein',
	'author_email' => 'typo3-ext(at)bitmotion.de',
	'author_company' => '<a href="http://www.bitmotion.de" target="_blank">bitmotion.de</a>',
	'constraints' => array(
		'depends' => array(
			'php' => '5.3.0-5.6.99',
			'typo3' => '6.2.0-7.6.99',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);
